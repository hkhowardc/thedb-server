#include <stdio.h>
#include <sys/time.h>
#include <time.h>

#include "argparse/argparse.h"
#include "socket/socketServer.h"
#include "structure/doubleLinkedList.h"
#include "structure/dynamicString.h"

// extern int yylex();
extern int yyparse();

static const char *const usages[] = {
    "./thedb-server [options] [[--] args]",
    "./thedb-server [options]",
    NULL,
};

int main(int argc, const char **argv) {
  int port = 169;
  struct argparse_option options[] = {
      OPT_HELP(),
      OPT_GROUP("Basic options"),
      OPT_INTEGER('p', "port", &port, "port number", NULL, 0, 0),
      OPT_END(),
  };

  struct argparse argparse;
  argparse_init(&argparse, options, usages, 0);
  argparse_describe(
      &argparse,
      "\nA brief description of what the program does and how it works.",
      "\nAdditional description of the program after the description of the "
      "arguments.");
  argc = argparse_parse(&argparse, argc, argv);
  printf("port: %d\n", port);

  // yyparse();
  printf("TheDB server\n");
  struct timeval stop, start;
  gettimeofday(&start, NULL);

  insertFirst(1, 10);
  insertFirst(2, 20);
  insertFirst(3, 30);
  insertFirst(4, 1);
  insertFirst(5, 40);
  for (int x = 0; x < 100; x++) {
    if (x % 2 == 0) {
      insertFirst(x, x);
    } else {
      insertLast(x, x);
    }
  }

  gettimeofday(&stop, NULL);
  printf("took %lu us\n",
         (stop.tv_sec - start.tv_sec) * 10 + stop.tv_usec - start.tv_usec);
  // printf("\nList (First to Last): ");
  // displayForward();
  // printf("\n");
  // printf("\nList (Last to first): ");
  // displayBackward();
  // printf("\nList , after deleting first record: ");
  // deleteFirst();
  // displayForward();
  // printf("\nList , after deleting last;. record: ");
  // deleteLast();
  // displayForward();
  // printf("\nList , insert after key(4) : ");
  // insertAfter(4, 7, 13);
  // displayForward();
  // printf("\nList , after delete key(4) : ");
  // delete (4);
  // displayForward();

  struct dynamicString ds;
  dsSet(&ds, "peter");
  printf("ds->len = %d\n", ds.len);
  printf("ds->free = %d\n", ds.free);
  printf("ds->buffer = %s\n", ds.buffer);

  int r = startSocketServer(port);
  if (r != 0) {
    printf("Socket server error = %d\n", r);
  }
  return 0;
}
